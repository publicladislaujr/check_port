const express = require('express');
const {spawn} = require('child_process');
const port = 3000;
const app = express();

app.use(express.json());

app.get('/', (req, res) => {
    res.send('Consulta Portas!');
}),

app.post('/checkport', (req, res) => {
    var dataToSend;

    const {host, porta} = req.body;

    if (host && porta) {

        const python = spawn('python', ['check_port.py',host,porta]);

        python.stdout.on('data', function (data) {

            console.log('Chamando dados do script ...');             
            dataToSend = data.toString();

            if (parseInt(dataToSend) === 0)
            {
                res.send('A porta '+ porta +' esta aberta');    
            } else {
                res.status(400).send('A porta '+ porta +' esta fechada' );
            }               
        });

        python.on('close', (code) => {
            console.log(`Fechando processos chamados com o codigo ${code}`);
        });
    } else {
        res.status(400).send('Favor verificar os dados informados para solicitação'); 
    }
});
app.listen(port, () => console.log(`Aplicação executando na porta: ${port}!`))