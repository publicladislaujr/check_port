FROM node:10-alpine

WORKDIR /app 
COPY . /app 

RUN apk update 
RUN apk add python  
RUN npm install
RUN npm install express

EXPOSE 3000 
CMD ["node", "index.js"]