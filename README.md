# Check Port

API para realizar chamada em endereço e porta para validar  a disponibilidade, basicamente foi criada com a idea de monitorar o enddereço.

Utilizado Java Scrip e Python criar a aplicação

Rodar em docker

docker build -t <conta-registry>/<nome-imagem>:<tag> <novo-diretório>
docker run --name <nome-container> --rm -p 3000:3000 <conta-registry>/<nome-imagem>:<tag>


Body que deve ser enviado para consulta via post 

{
	"host" : "url",
	"porta": "port",
	"timeout" : "5"
}

URL: http://localhost:3000/checkport